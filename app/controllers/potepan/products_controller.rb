class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_NUMBER_COUNT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxon_ids.first || Spree::Taxon.ids.first
    @related_products = @product.related_products.limit(RELATED_PRODUCTS_MAX_NUMBER_COUNT)
  end
end
