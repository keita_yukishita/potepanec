module Potepan::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).includes(master: [:default_price, :images]).distinct
  end

  Spree::Product.prepend self
end
