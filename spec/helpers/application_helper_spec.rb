require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ApplicationHelper. For example:
#
# describe ApplicationHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title methodについて" do
    include ApplicationHelper
    context "page_titleに戻り値が無い時" do
      page_title = ""
      it { expect(full_title(page_title)).to eq "BIGBAG Store" }
    end

    context "page_titleがnilの時" do
      it { expect(full_title(nil)).to eq "BIGBAG Store" }
    end

    context "page_titleに戻り値がある時" do
      it { expect(full_title(Rails)).to eq "Rails|BIGBAG Store" }
    end
  end
end
