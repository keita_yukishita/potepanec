require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let(:other_taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  let(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  let(:other_related_products) { create_list(:product, 4, taxons: [other_taxon]) }

  describe "関連商品の値をチェックする" do
    context "関連する商品がある時" do
      it "最大4個まで値を持つ" do
        expect(related_products.size).to eq 4
      end
    end

    context "関連する商品が無い時" do
      it "@related_productsは他のTaxonを持たない" do
        expect(related_products).not_to eq other_related_products
      end
    end
  end
end
