require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  # カテゴリーページへ事前にアクセス
  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "商品とタイトルの表示が正しいか" do
    within ".page-title" do
      expect(page).to have_title taxon.name
    end
    within ".productBox" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_link product.name
    end
  end

  scenario "商品詳細ページへの正しいリンクが設定されているか" do
    within ".productBox" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  scenario "サイドバーの動作が正しいか" do
    within ".side-nav" do
      click_on taxonomy.name
      taxonomy.taxons.each do |taxon|
        expect(page).to have_content taxon.name
        expect(taxon.products.count).to eq taxon.products.count
      end
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
