require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], price: 123) }
  let!(:other_product) { create(:product, taxons: [taxon], price: 321) }

  # 商品ページへ事前にアクセス
  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細の表示が正しいか" do
    within '.media-body' do
      expect(page).to have_title product.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_link "カートへ入れる"
      expect(page).to have_link "一覧ページへ戻る"
    end
  end

  scenario "トップページへ正しいリンクが設定されているか" do
    within '.nav' do
      click_on 'Home'
      expect(current_path).to eq potepan_root_path
    end
  end

  feature "一覧ページのリンクが正しいか" do
    scenario "taxons.idsに値がある時" do
      within '.taxon-link' do
        click_on '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(product.taxons.ids.first)
      end
    end

    scenario "taxons.idsに値が無い時" do
      product.taxons = []
      within '.taxon-link' do
        click_on '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end

  scenario "関連商品の表示とリンクが正しいか" do
    within all('.productBox')[0] do
      expect(page).to have_content other_product.name
      expect(page).not_to have_content product.name
      expect(page).to have_content other_product.display_price
      expect(page).not_to have_content product.display_price
      expect(find('a')[:href]).to eq potepan_product_path(other_product.id)
    end
  end

  scenario "関連商品のリンク経由で商品詳細画面が適切に表示されるか" do
    click_on other_product.name
    within '.media-body' do
      expect(page).to have_title other_product.name
      expect(page).to have_content other_product.name
      expect(page).to have_content other_product.display_price
      expect(page).to have_content other_product.description
      expect(page).to have_link "カートへ入れる"
      expect(page).to have_link "一覧ページへ戻る"
    end
  end
end
