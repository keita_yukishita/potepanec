require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before { get "/potepan/categories/#{taxon.id}" }

    it "正常にレスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "正しい分類の名前が返されるか" do
      expect(response.body).to include taxonomy.name
    end

    it "正しいカテゴリー名が返されるか" do
      expect(response.body).to include taxon.name
    end

    it "正しい商品名が返されるか" do
      expect(response.body).to include product.name
    end

    it "正しい価格が返されるか" do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
